package xmlstream_test

import (
	"context"
	"encoding/xml"
	"fmt"
	"os"
	"sync"

	"gitlab.com/lercher/xmlstream"
)

type NonSimple struct {
	AString  string
	Elements xmlstream.Junction[NonSerializeable]
	BString  string
}

type NonSerializeable struct {
	MyMap map[int]int // maps can't be serialized as xml
}

func ExampleJunction_withCancelCause() {
	ctx, cc := context.WithCancelCause(context.Background())

	ns := NonSimple{
		"This is marshalled",
		xmlstream.New[NonSerializeable](cc),
		"This is never reached",
	}

	wg := new(sync.WaitGroup)
	wg.Add(1)
	go func() {
		defer wg.Done()
		defer ns.Elements.Close()
		for i := 0; i < 10; i++ {
			fmt.Println("posting item", i+1) // #1 #2 #3 most often, or #1 #2 if this func is slow
			// #1 posted normally, is received, but generates an error so the receiver calls cc and goes into channel draining mode
			// #2 gets posted b/c #1 was received and we have 1 slot in the channel
			// #3 gets posted b/c cc also involves async channel operations on the context ctx
			ns.Elements.Post(NonSerializeable{})
			if ctx.Err() != nil {
				fmt.Println(ctx.Err()) // context canceled
				break
			}
		}
	}()

	enc := xml.NewEncoder(os.Stdout)
	enc.Indent("", "  ")
	err := enc.Encode(ns)
	wg.Wait()
	if err != nil {
		// encoding: after item 3: streaming item 1: xml: unsupported type: map[int]int
		fmt.Println("encoding:", err)
	}

	if ctx.Err() != nil {
		// context: streaming item 1: xml: unsupported type: map[int]int
		fmt.Println("context:", context.Cause(ctx))
	}

	// Output:
	// posting item 1
	// posting item 2
	// posting item 3
	// context canceled
	// encoding: after item 3 when streaming item 1: xml: unsupported type: map[int]int
	// context: streaming item 1: xml: unsupported type: map[int]int
}
