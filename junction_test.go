package xmlstream_test

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"testing"

	"gitlab.com/lercher/xmlstream"
)

func TestJunction(t *testing.T) {
	s := Simple{
		"Pardon me, boy, is that the Chattanooga Choo Choo?",
		xmlstream.New[Element](nil),
		"Boy, you can give me a shine.",
		"Can you afford to board the Chattanooga Choo Choo?",
		"I've got my fare, and just a trifle to spare.",
	}

	go func() {
		for i := 0; i < 3; i++ {
			txt := fmt.Sprintf("Yes, yes, Track %v!", 29+i)
			s.Elements.Post(Element{i + 1, txt})
		}
		s.Elements.Close()
	}()

	buf := new(bytes.Buffer)
	enc := xml.NewEncoder(buf)
	enc.Indent("", "  ")
	err := enc.Encode(s)
	if err != nil {
		t.Fatal(err)
	}

	want := choochoo
	got := string(buf.Bytes())

	if want != got {
		t.Errorf("\nwant: %v\ngot:  %v", want, got)
	}
}

const choochoo = `<Simple>
  <AString>Pardon me, boy, is that the Chattanooga Choo Choo?</AString>
  <elements>
    <element>
      <ItemNumber>1</ItemNumber>
      <ItemText>Yes, yes, Track 29!</ItemText>
    </element>
    <element>
      <ItemNumber>2</ItemNumber>
      <ItemText>Yes, yes, Track 30!</ItemText>
    </element>
    <element>
      <ItemNumber>3</ItemNumber>
      <ItemText>Yes, yes, Track 31!</ItemText>
    </element>
  </elements>
  <BString>Boy, you can give me a shine.</BString>
  <CString>Can you afford to board the Chattanooga Choo Choo?</CString>
  <DString>I&#39;ve got my fare, and just a trifle to spare.</DString>
</Simple>`
