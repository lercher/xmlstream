# xmlstream [![Go Reference](https://pkg.go.dev/badge/gitlab.com/lercher/xmlstream.svg)](https://pkg.go.dev/gitlab.com/lercher/xmlstream)

xmlstream is a Go package to solve this one problem:
If you need to XML serialize massive repeated content inside
a container it is often not a good idea to accumulate
a large slice of some type only to then serialize this
large struct. We do this by posting to a channel in second
Go-routine.

We want to use the bullet-proof Go standard lib provided xml marshaling
procedures as much as possible so we provide a new data-type that
can be included and tagged in the container Go struct as a normal field.

This type implements custom xml marshaling on the one side and a channel of T
of capacity one on the other side. Then two go-routines need to work
together: one, e.g. the main Go routing, calling standard marshalling
on the container struct, and a second one posting T data into the
channal during the marshalling. When the custom marshalling code is called
by Go's standard marshaller a loop over the channel is processed
which marshals each received item of type T to the output writer until
the channel is closed. Standard xml marshaling then again takes control of
the processing.

Note: this package is only marshaling to xml and not unmarshaling
large xml byte streams. It might be not too hard to transfer the kernel
idea to JSON marshalling. The other way round, i.e. unmarshaling sth
which is then posted to a channel, might be sth that can be realized
separately. However, we expect this to be hard on the level of details, so,
we don't persue unmashalling right now.

Note: this package depends only on the Go standard library. The code itself
is not that long but tricky on the details level.

## Usage

```sh
go get gitlab.com/lercher/xmlstream
```

See the go doc examples for up-to-date usage guidance.
Another good idea is to look at the tests. The most basic
use case is something like this:

```go
import "gitlab.com/lercher/xmlstream" // et.al.

type Simple struct {
    AString  string
    Elements xmlstream.Junction[Element] // declare that we emit lots of Element-s
    BString  string
}

type Element struct {
    ItemText string
}

func main() {
    s := Simple{
        "Pardon me, boy, is that the Chattanooga Choo Choo?",
        xmlstream.New[Element](nil), // initialize the Junction without a CancelCauseFunc
        "Boy, you can give me a shine.",
    }

    // in a separate Go routine, Post() Element-s to the Junction
    // until the Junction is Close-d.
    go func() {
        defer s.Elements.Close()
        for i := 0; i < 3; i++ {
            txt := fmt.Sprintf("Yes, yes, Track %v!", 29+i)
            s.Elements.Post(Element{i + 1, txt})
        }
    }()

    // xml marshal the container s:
    enc := xml.NewEncoder(os.Stdout)
    enc.Indent("", "  ")
    err := enc.Encode(s)
    if err != nil {
        fmt.Println(err)
    }
}
```

### Important

This package is designed for exactly one `Junction[T]` in the whole type
hierarchy of the container struct to be xml serialized. It may or may not
work with more than one junction sequentially, junctions inside the item types
of other junction fields, or even with recursive types. However, using such a
type _will_ be complicated, b/c you need to know exactly during encoding
the container, when to asynchronously:

- create junction instances with `New()`
- `Post()` to a particular junction instance (b/c the channels block with capacity one)
- `Close()` this particular junction instance to advance to the next junction

Best approach IMO might be to create all junctions in advance, and then set-up
a dedicated Go-routine for each such junction instance that posts and closes.

### Another Word of Warning

It is imperative to `Close()` the junction and hence the internal channel, even
after context canceling on marshalling errors. If it's not called, the
marshalling call _will_ block forever.

## License

This lib is licensed under the [MIT license](LICENSE).

## Project status

This project is _incubating_, but it is not that complicated matter.
So, we expect to reach something useable very soon.

## Variant Architecture

As an alternative you can partially write the container xml
like a header into an `io.Writer` then repeatedly marshal the
repeated struct and finally write as footer the rest of the
container xml. This might be even more efficient than this
approach. However, I guess, it's harder to implement and you
loose xml serializasion struct tagging for the container and
maybe it's hard to do pretty printing well.

In this package's implementation this is all done by the
standard library code.

## Ressources

- <https://stackoverflow.com/questions/30928770/marshall-map-to-xml-in-go>
  for an xml marshaller for Go maps.
- <https://bitfieldconsulting.com/golang/examples>
  on how to make godoc examples.
