package xmlstream_test

import (
	"context"
	"encoding/xml"
	"testing"

	"gitlab.com/lercher/xmlstream"
)

func TestNew(t *testing.T) {
	ctx, cc := context.WithCancelCause(context.Background())

	// element-type map is not xml serializable so we get an error during Marshal and that goes to cc, too
	j := xmlstream.New[map[string]string](cc)

	j.Post(nil) // chan has size 1 so it won't block
	j.Close()   // if not closed xml.Marshal(j) blocks forever
	_, err := xml.Marshal(j)
	if err != nil {
		// that's what we want
		{
			got := err.Error()
			want := "after item 1 when streaming item 1: xml: unsupported type: map[string]string"
			if got != want {
				t.Errorf("wanted error %q, got %q", want, got)
			}
		}
		{
			cause := context.Cause(ctx)
			want := "streaming item 1: xml: unsupported type: map[string]string"
			if cause.Error() != want {
				t.Errorf("wanted cause %q, got %q", want, cause.Error())
			}
		}
		if ctx.Err() == nil {
			t.Error("wanted ctx canceled, got nil-error")
		}
	} else {
		t.Error("wanted marshal error, got nil")
	}
}
