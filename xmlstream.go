// Package xmlstream provides the type Junction[T]
// to allow xml marshaling of a container with
// a large repeated element via posting
// to a channel instead of providing a massive
// slice of some type only to serialize it.
package xmlstream

import (
	"context"
)

// New creates a new Junction with an underlying
// channel of T of size one. The CancelCauseFunc cc
// can be nil. If it is non-nil it is called if an
// error happens during serialization of a T item.
func New[T any](cc context.CancelCauseFunc) Junction[T] {
	j := Junction[T]{
		c:  make(chan T, 1),
		cc: cc,
	}
	return j
}
