package xmlstream_test

import (
	"encoding/xml"
	"fmt"
	"os"

	"gitlab.com/lercher/xmlstream"
)

type Simple struct {
	AString  string
	Elements xmlstream.Junction[Element] `xml:"elements>element"` // note that both are honored
	BString  string
	CString  string
	DString  string
}

type Element struct {
	ItemNumber int
	ItemText   string
}

func ExampleJunction_post() {
	s := Simple{
		"Pardon me, boy, is that the Chattanooga Choo Choo?",
		xmlstream.New[Element](nil),
		"Boy, you can give me a shine.",
		"Can you afford to board the Chattanooga Choo Choo?",
		"I've got my fare, and just a trifle to spare.",
	}

	go func() {
		defer s.Elements.Close()
		for i := 0; i < 3; i++ {
			txt := fmt.Sprintf("Yes, yes, Track %v!", 29+i)
			s.Elements.Post(Element{i + 1, txt})
		}
	}()

	enc := xml.NewEncoder(os.Stdout)
	enc.Indent("", "  ")
	err := enc.Encode(s)
	if err != nil {
		fmt.Println(err)
	}

	// Output:
	// <Simple>
	//   <AString>Pardon me, boy, is that the Chattanooga Choo Choo?</AString>
	//   <elements>
	//     <element>
	//       <ItemNumber>1</ItemNumber>
	//       <ItemText>Yes, yes, Track 29!</ItemText>
	//     </element>
	//     <element>
	//       <ItemNumber>2</ItemNumber>
	//       <ItemText>Yes, yes, Track 30!</ItemText>
	//     </element>
	//     <element>
	//       <ItemNumber>3</ItemNumber>
	//       <ItemText>Yes, yes, Track 31!</ItemText>
	//     </element>
	//   </elements>
	//   <BString>Boy, you can give me a shine.</BString>
	//   <CString>Can you afford to board the Chattanooga Choo Choo?</CString>
	//   <DString>I&#39;ve got my fare, and just a trifle to spare.</DString>
	// </Simple>
}
