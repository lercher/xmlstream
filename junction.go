package xmlstream

import (
	"context"
	"encoding/xml"
	"fmt"
)

// Junction combines a stream of T provided by calling Post()
// with custom xml marshaling. When
// some container struct having a Junction[T] as a field
// is serialized, the marshaling goroutine loops over
// the Junction's channel and serializes every received T
// item to the output writer.
//
// If an error happens during marshalling of a Post-ed
// T item, the Junctions CancelCauseFunc is called
// unless it is nil. It is valid to operate without a
// CancelCauseFunc, however, this is normally not very useful.
//
// Note that it may take some time for the attached context
// to have an observeable non-nil Err().
// If such an error happens the channel receiver loop goes
// into "draining mode", i.e. it just receives from the channel
// until it is Close-ed, finally. I.e. until the Post-ing code
// has observed and reacted properly on the attached context's Err()
// or Done() signal by closing the Junction.
type Junction[T any] struct {
	c  chan (T)
	cc context.CancelCauseFunc
}

// Post enqueues a new T to be marshalled as xml.
// Post must be called in a different Go routine from
// the Go routine marshalling sth containing this Junction.
// In case an xml marshalling error happens on a T item
// asynchronously, subsequent posted it-s will be received
// but ignored. Post thus blocks only if the output writer
// blocks during marshalling, or marshalling is not (yet) running.
// Don't forget to Close the Junction after all relevant T items
// were Post-ed.
func (j *Junction[T]) Post(it T) {
	j.c <- it
}

// Close closes the underlying channel of T.
// It denotes the end of data to be streamed.
// Xml marshalling then continues marshaling
// the remaining fields of the container struct which
// associated this Junction instance.
func (j *Junction[T]) Close() {
	close(j.c)
}

// MarshalXML implememts custom xml marshalling.
// Junction[T] marshals posted T items into xml
// until it is closed. If it is never closed, MarshalXML
// won't return, of course.
func (j Junction[T]) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	var err error
	n := 0
	for t := range j.c {
		n++
		if err != nil {
			// we just drain the channel by dropping all receives.
			// but we hope that the cc call will stop Post-ing and
			// finally Close() is called
			continue
		}
		// marshal the received T item
		err = e.EncodeElement(t, start)
		if err != nil {
			// 1st error only
			err = fmt.Errorf("streaming item %d: %w", n, err)
			if j.cc != nil {
				// notify context of the error
				j.cc(err)
			}
			// as err is non-nil now, we just drain the channel until it is closed
		}
	}
	if err != nil {
		return fmt.Errorf("after item %d when %w", n, err)
	}
	return nil
}
